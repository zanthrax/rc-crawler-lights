#include <SPI.h> // required for SD.h
#include <SD.h> // to access SD card
#include <TMRpcm.h> // plays wav files

// pins
const int sdChipSelectPin = 10;  // arduino pin connected to SD adapter CS pin
const int audioPin = 9; // speaker for sound output
const int mute = 8; // output for amp muting
const int throttlePin = 18; // throttle input from rX // 5
const int hornPin = 17; // horn input from rX // 6
const int reversePin = 7; // output for reverse lights
const int beaconPin = 3; // output for switching beacon modes

// config
const bool debug = false; // enables serial i/o
const int throttleCenter = 1500;
const int hornCenter = 1500;
const int deadZone = 100;
const int volume = 4; // audio volume (1 to 7)

// program vars
unsigned long throttlePosition;
unsigned long hornPosition;
bool isReversing;
File dir;
TMRpcm audio;

void setup() {
  pinMode(hornPin, INPUT);
  pinMode(throttlePin, INPUT);
  pinMode(reversePin, OUTPUT);
  pinMode(beaconPin, OUTPUT);
  pinMode(mute, OUTPUT);

  digitalWrite(reversePin, false);
  digitalWrite(beaconPin, false);
  digitalWrite(mute, false);

  // Open serial communications
  if (debug) {
    Serial.begin(9600);
  }

  // open SD card connection
  if (!SD.begin(sdChipSelectPin)) {
    if (debug) {
      Serial.println("SD fail");
    }
    return;   // stop
  }

  // list files in root of card
  if (debug) {
    dir = SD.open("/");
    while (true) {
      File entry =  dir.openNextFile();
      if (!entry) {
        break;
      }
      Serial.println(entry.name());
      entry.close();
    }
  }

  // startup sound
  audio.speakerPin = audioPin;
  audio.setVolume(volume);
  audio.play("startup.wav");

  if (debug) {
    Serial.println("Setup complete");
  }
}

void loop() {
  // do nothing if we're already playing sound.
  // this prevents interference to the audio library
  if (audio.isPlaying()) {
    //audio.setVolume(volume);
    digitalWrite(mute, false);
    delay(100);
    return;
  } else {
    //audio.setVolume(0);
    digitalWrite(mute, true);
  }

  // read inputs
  delay(30);
  throttlePosition = pulseIn(throttlePin, HIGH, 25000);
  delay(30);
  hornPosition = pulseIn(hornPin, HIGH, 25000);

  if (!throttlePosition && !hornPosition) {
    if (debug) {
      Serial.println("No RC input");
    }
    return;
  }

  // filter out invalid inputs
  if (throttlePosition < deadZone || throttlePosition > 2500) {
    throttlePosition = 0;
  }
  if (hornPosition < deadZone || hornPosition > 2500) {
    hornPosition = 0;
  }

  // reversing sound
  isReversing = false;
  if (throttlePosition) {
    if (throttlePosition < (throttleCenter - deadZone)) {
      isReversing = true;
      if (debug) {
        Serial.println("Reversing");
      }
      digitalWrite(reversePin, true);
      //audio.play("re_beep.wav"); // 1875Hz beep
      audio.play("re_noise.wav"); // white noise
    } else {
      digitalWrite(reversePin, false);
    }
  }
  // reverse lights


  // horns
  if (hornPosition) {
    if (hornPosition < (hornCenter - deadZone)) {
      if (debug) {
        Serial.println("Honk 1");
      }
      audio.play("horn1.wav");
    }
    if (hornPosition > hornCenter + deadZone) {
      if (debug) {
        //Serial.println("Honk 2");
        Serial.println("Beacons toggle");
      }
      //audio.play("horn2.wav");
      digitalWrite(beaconPin, !digitalRead(beaconPin));   
      delay(500); // debounce   
    }
  }  

  // debugging info
  if (debug) {
    Serial.print("T "); // Throttle
    Serial.print(String(throttlePosition));
    Serial.print(" H "); // Throttle
    Serial.print(String(hornPosition));
    Serial.print(" A "); // Throttle
    Serial.print(audio.isPlaying() ? "1" : "0");

    Serial.println("");
  }
}
